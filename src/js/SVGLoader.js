import $ from 'jquery';

export default class SVGLoader {
    /**
     * @param  {String} target - selector for elements to fetch and replace
     * @param  {String} urlAttr - atribute with url
     */
    static fetch(target, urlAttr) {
        return new Promise((resolve, reject)=>{
            let $tar = $(target);

            $tar.each((i,e)=>{
                let $self = $(e);
                let target = $self.attr(urlAttr)
                let clas = $self.attr("class");
                $.ajax({
                    method: "GET",
                    url: target,
                    dataType: "html"
                }).done((result)=>{
                    $self.replaceWith($(result).addClass(clas));
                }).fail((jq, msg)=>{
                    console.log(msg);
                });
            });

            resolve();
        });
    }
    /**
     * @description fetches all .svg files in img src
     */
    static autoFetch() {
        let regex = /\.(svg)$/i;
        let $imgs = $("img").filter((i,e)=>{
            return regex.test($(e).attr("src"))
        });
        return this.fetch($imgs, "src");
    }
}