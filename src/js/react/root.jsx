import ReactDOM from 'react-dom';
import { KnowledgeBoxes } from './components/KnowledgeBoxes';

export function startReacting() {
	ReactDOM.render(KnowledgeBoxes, document.getElementById('react'))
}