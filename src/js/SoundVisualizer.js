import $ from 'jquery'
import {IHowl, Howl, Howler} from 'howler';
import { request } from 'http';
import StaticHelpers from './StaticHelpers.js';



export default class SoundVisualizer {
    /**
     * @param  {JQuery} $el
     * @param {IHowl} sound
     */
    constructor($el, sound) {
        this.template = `
            <div class="canvas"></div>
            <div class="controls">
                <span class="playPause paused"></span>
                <span class="volume">
                    <input id="particlesize" type="range" class="custom-control-range" min="0" max="100" step="1">
                </span>
                <span class="speaker">
                    <span></span>
                </span>
            </div>
        `;

        //set control
        this.control = $el;
        this.control.empty();
        this.control.append(this.template);

        this.playControl = $el.find(".playPause");
        this.volumeControl = $el.find(".volume input");
        this.muteControl = $el.find(".speaker");
        this.soundReference = sound;
        this.animationFrame;
        this.isMuted = false;

        //set analyzer
        this.soundAnalyser = Howler.ctx.createAnalyser();
        Howler.masterGain.connect(this.soundAnalyser);
    
        let self = this;
        //sets pars
        this.bars = {
            width: 3,
            space: 1,
            count: self.soundAnalyser.fftSize
        };

        //starting
        this.spawnCanvas();
        this.recalculateBars();
        this.bindEvents($("body"));
        this.setVolume(50);
    }
    respawn($el) {
        this.control = $el;
        this.control.empty();
        this.control.append(this.template);

        
        this.playControl = $el.find(".playPause");
        this.volumeControl = $el.find(".volume input");
        this.muteControl = $el.find(".speaker");

        //starting
        this.spawnCanvas();
        this.recalculateBars();
        this.setVolume();
        
        if (this.soundReference.playing(this.playId)) {
            this.soundReference.pause();
            this.stopDraw();
            this.toggleMusic();
        }
    }
    /**
     * @param  {IHowl} sound
     */
    setSound(sound) {
        let oldVol = this.soundReference.volume;
        this.soundReference = sound;
        this.soundReference.volume = oldVol;
        this.soundReference.mute(this.isMuted);
    }
    /**
     * @param  {JQuery} $body
     */
    bindEvents($body) {
        $body.on("click", ".soundvisual .playPause", (e)=>{
            this.toggleMusic();
        });
        $body.on("change", ".soundvisual .volume input", (e)=>{
            this.setVolume(this.volumeControl.val());
        });
        $body.on("click", ".soundvisual .speaker", (e)=> {
            if (this.muteControl.hasClass("mute")) {
                this.muteControl.removeClass("mute");
                this.isMuted = false;
                this.soundReference.mute(this.isMuted);
            } else {
                this.muteControl.addClass("mute");
                this.isMuted = true;
                this.soundReference.mute(this.isMuted);
            }
        });
    }
    disablePlayPause() {
        this.playControl.addClass("disabled");
    }
    enablePlayPause() {
        this.playControl.removeClass("disabled");
    }
    toggleMusic() {
        if (this.playControl.hasClass("paused")) {
            this.play();
        } else {
            this.pause();
        }
    }
    play() {
        this.stopDraw();
        this.playControl.removeClass("paused");
        this.playId = this.soundReference.play();
        this.draw();
    }
    pause() {
        this.playControl.addClass("paused");
        this.soundReference.pause();
        this.stopDraw();
    }
    stop() {
        this.pause();
        this.soundReference.stop();
    }
    /**
     * @param  {Number} vol?
     */
    setVolume(vol = -1) {
        if (vol == -1) {
            this.volumeControl.val(this.soundReference.volume() * 100)
        } else {
            this.soundReference.volume(vol / 100);
        }
    }
    spawnCanvas() {
        this.control.find(".canvas").empty().append("<canvas></canvas>");
        this.canvas = this.control.find("canvas")[0];
        this.canvas.width = 256;
        this.canvas.height = 100;
        this.canvasCtx = this.canvas.getContext("2d");
        this.canvasCtx.fillStyle = "#EAEAEA";
    }
    draw() {
        let self = this;
        this.animationFrame = window.requestAnimationFrame(()=>{
            self.draw();
        });
        let fbc_array = new Uint8Array(this.soundAnalyser.frequencyBinCount);
        this.soundAnalyser.getByteFrequencyData(fbc_array);
        this.canvasCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        //console.log(fbc_array);
        
        let spacing = this.bars.width + this.bars.space;
        let bar_width = this.bars.width;
        for (let i = 0; i < this.bars.count; i++) {
            let bar_x = i * spacing;
            let bar_height = -(StaticHelpers.rescaleInt(fbc_array[i], 255, this.canvas.height));
            this.canvasCtx.fillRect(bar_x, this.canvas.height, bar_width, bar_height);
        }
    }
    stopDraw() {
        window.cancelAnimationFrame(this.animationFrame);
        this.canvasCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
    recalculateBars() {
        let cWith = this.canvas.width;
        let barSpace = this.bars.width + this.bars.space;
        this.bars.count = StaticHelpers.nearestPow(2, cWith / barSpace) > 32 ? StaticHelpers.nearestPow(2, cWith / barSpace) : 64;
        this.soundAnalyser.fftSize = this.bars.count * 2;
    }
}