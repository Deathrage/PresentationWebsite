import $ from 'jquery'

export default class StaticHelpers {
    static ensureJquery(element = $(document)) {
        if (element instanceof $) {
            return element;
        } else if (element instanceof HTMLElement) {
            return $(element);
        } else {
            throw new Error("Not a jQuery object nor HTMLElement");
        }
    }
    static setDeepProperty(object, path, value) {
        var schema = object;  
        var pList = path.split('.');
        var len = pList.length;
        for (var i = 0; i < len - 1; i++) {
            var elem = pList[i];
            if (!schema[elem]) schema[elem] = {}
            schema = schema[elem];
        }
        schema[pList[len - 1]] = value;
    }
    static getDeepProperty(object, path) {
        var schema = object;  
        var pList = path.split('.');
        var len = pList.length;
        for (var i = 0; i < len - 1; i++) {
            var elem = pList[i];
            if (!schema[elem]) schema[elem] = {}
            schema = schema[elem];
        }
        return schema[pList[len - 1]];
    }
    static subtractJquery(firstAr, secondAr) {
        let diff = [];
        $.grep(secondAr, function(el) {
            if ($.inArray(el, firstAr) == -1) diff.push(el);
        });
        return diff;
    }
    static getQueryStringDictionary(url = window.location.href) {
        return url.replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
    }
    static getQueryString(name, url = window.location.href) {
        name = name.replace(/[[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    /**
     * @description Returns integer in specified bounds
     * @param  {Number} min
     * @param  {Number} max
     */
    static randomBetween(min = 0, max = 1) {
        return Math.floor(Math.random() * max) + min;  
    }
    static nearestPow(power, int) {
        return Math.pow( power, Math.round( Math.log( int ) / Math.log( power ) ) ); 
    }
    static rescaleInt(int, orgScale, newScale) {
        return (int / orgScale) * newScale;        
    }
}