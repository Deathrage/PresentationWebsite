import $ from 'jquery'
import nunjucks from 'nunjucks'
import { pJS } from 'particles.js'
import StaticHelpers from './StaticHelpers.js'
import SoundBoard from './SoundBoard.js'
import 'bootstrap-colorpicker'

export default class App {
    constructor($app) {
        this.app = StaticHelpers.ensureJquery($app);
        this.queryStrings = StaticHelpers.getQueryStringDictionary(window.location.href);
        //wipe app clean
        this.app.html("");
        this.soundBoard = null;
        this.pJSRef = new Object();
        this.pJSParams = new Object();
        this.confDrops = new Object();
    }
    /**
     * @param  {boolean} fixed=false
     */
    toggleScroll(fixed = false) {
        let $tars = $("body, html");
        if ($tars.hasClass("noscroll") || $tars.hasClass("noscroll-fixed")) {
            $tars.removeClass("noscroll"); 
            $tars.removeClass("noscroll-fixed");
            return;
        }
        if (fixed == true) {
            $tars.addClass("noscroll-fixed");
        } else {
            $tars.addClass("noscroll");
        }
    }
    
    /**
     * @param  {JQuery} $tag
     */
    spawnParticles($tag) {
        $tag.append("<canvas id='canvas'></canvas>")
        this.pJSRef = new pJS("canvas", this.pJSParams);
    }
    /**
     * @param  {JQuery} $tag
     */
    removeParticles($tag) {
        $tag.empty();
        this.pJSRef.particlesStop();
        this.pJSRef = null;
    }
    /**
     * @param  {JQuery} $tag
     */
    respawnParticles($tag){
        this.removeParticles($tag);
        this.spawnParticles($tag);
    }

    
    /**
     * @param  {Object} config
     */
    loadParticleConfigurator(config) {
        let $el = StaticHelpers.ensureJquery(config);
        let $inputs = $el.find("input, select");
        $inputs.each((i, el) => {
            let $self = $(el);
            let adress = this.getObjectAdress($self, "data-key");
            let val = StaticHelpers.getDeepProperty(this.pJSParams, adress);
            // console.log(adress);
            // console.log(val);
            if ($self.is(":checkbox")) {
                $self.prop('checked', val);
            } else if ($self.parent().hasClass("custom-picker")) {
                $self.siblings(".value").css("background-color", val);
                $self.colorpicker({"color": val, useAlpha: false});
                // console.log($self);
            } else if ($self.is("select")) {
                $self.val(val);
                if ($self.hasClass("shape-select") && $self.val() == "polygon") {
                    $self.next("ul").slideDown();
                } else if ($self.hasClass("shape-select")) {
                    $self.next("ul").slideUp();
                }
            } else if ($self.attr("type") == "range") {
                $self.siblings(".value").text(val);
                $self.val(val);
            } 
        });
    }
    /**
     * @param  {JQuery} $body
     */
    particlesConfigEvents($body) {
        $body.on("change", ".particle-config input, .particle-config select", (e)=>{
            console.log(e.target);
            let $tar = $(e.target);
            let val = null;
            if ($tar.attr("type") == "range") {
                val = Number($tar.val());
                $tar.siblings(".value").text(val);
            } else if ($tar.parent().hasClass("custom-picker")) {
                val = $tar.val();
                $tar.siblings(".value").css("background-color", val);
            } else if ($tar.is("select")) {
                val = $tar.val();
    
                if ($tar.hasClass("shape-select") && $tar.val() == "polygon") {
                    $tar.next("ul").slideDown();
                } else if ($tar.hasClass("shape-select")) {
                    $tar.next("ul").slideUp();
                }
    
            } else if ($tar.is(":checkbox")) {
    
                val = $tar.prop('checked');
            }
            if (val != null) {
                let adress = this.getObjectAdress($tar, "data-key");
                StaticHelpers.setDeepProperty(this.pJSParams, adress, val)
                this.respawnParticles($(".js-app >  .canvas"));
            }
    
            return false;
        });
        $body.on("click", ".particle-config .custom-picker span", (e)=>{
            // console.log(e.target);
            $(e.target).siblings("input").trigger("focus");
            return false;
        });
        $body.on("click", ".particle-config .is-dropdown", (e)=>{
            e.stopPropagation();
            // console.log(e.target);
            let $tar = $(e.target);    
            let $truTar;
            if ($tar.hasClass("is-dropdown")) {
                $tar.children("ul").slideToggle();
                $truTar = $tar;
            } else if ($tar.siblings("ul").length > 0 && $tar.parent().hasClass("is-dropdown")){
                $tar.siblings("ul").slideToggle();
                $truTar = $tar.parent(".is-dropdown");
            }
            if ($truTar != null) {
                $truTar.siblings(".is-dropdown").children("ul").slideUp();
            }
        });
    }
    
    initSlideHover() {
        let $els = $(".slide-hover");
        $els.append("<span class='inverse'>" + $(".slide-hover").attr("data-text") + "</span>");
    }
    /**
     * @param  {JQuery} $body
     */
    bindSliderHoverEvents($body) {
        $body.on("mouseenter", ".particle-config .slide-hover:not(.is-open)", (e)=>{
            let $tar = $(e.target);
            if (!$tar.hasClass("slide-hover")) {
                $tar = $tar.parent(".slide-hover");
            }
            console.log($tar);
            if ($tar.hasClass("slide-hover")) {
                $tar.find(".inverse").stop().animate({"padding-right": "4px", "width": "100%"}, 500);
            }
             return false;
        }).on("mouseleave", ".particle-config .slide-hover:not(.is-open)", (e)=>{
            let $tar = $(e.target)
            if (!$tar.hasClass("slide-hover")) {
                $tar = $tar.parent(".slide-hover");
            }
            console.log($tar);
            if ($tar.hasClass("slide-hover")) {
                $tar.find(".inverse").stop().animate({"padding-right": "0", "width": "0"}, 500);
            }
            return false;
        });
        $body.on("click", ".particle-config .slide-hover", (e)=>{
            let $tar = $(e.target);
            if (!$tar.is("h2")) {
                $tar= $tar.parent("h2");
            }
            if ($tar.is("h2")) {
                $tar.toggleClass("is-open");
                $tar.siblings("ul").slideToggle(()=>{
                    // console.log("eee");
                    // if (!$tar.siblings("ul").is(":visible")) {
                    //     $tar.find(".inverse").stop().animate({"padding-right": "0", "width": "0"}, 500);;
                    // }
                });
            }
        });
    }
    
    /**
     * @param  {JQuery} $obj
     */
    getObjectAdress($obj) {
        let $parents = $obj.add($obj.parentsUntil(".option-list"));
        let adressAr = [];
        $parents.each((i, el) => {
            adressAr.push($(el).attr("data-key"));
        });
        adressAr = adressAr.filter(x => x != null && x.trim() != "");
        return adressAr.join(".");
    }
    /**
     * @param  {SoundBoard} board
     */
    loadSoundBoard(board) {
        if (board instanceof SoundBoard) {
            this.soundBoard = board;
        } else {
            throw new Error("Not a soundboard");
        }
    }
    
    rednerContent(templatePath, localizedContext, callback) {
        let appHtml = nunjucks.render(templatePath, localizedContext);
        this.app.html(appHtml);

        $(document).trigger("render");

        if (callback != null) {
            callback.call(this);
        }
    }
    hideApp(speed) {
        return new Promise((resolve, reject) => {
            this.app.stop();
            this.app.animate({ "opacity": 0 }, speed, resolve);
        });
    }
    showApp(speed) {
        return new Promise((resolve, reject)=>{
            this.app.stop();
            this.app.animate({ "opacity": 1 }, speed, resolve);
        });
    }
    hideTeaser(element = new $) {
        StaticHelpers.ensureJquery(element).hide();
    }
}