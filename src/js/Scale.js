import $ from "jquery";

export default class Scale {
    /**
     * @param  {any} $tar - HTML element, jQuery element or CSS selector
     * @param {Number} space - space between ruler marks in pixels
     */
    constructor($tar, space) {
        this.element = $($tar);
        this.spacing = space;

        this.dirEnum = {
            ltr: "ltr",
            rtl: "rtl",
            ttd: "ttd",
            btt: "btt"
        }
    }
    get barWidth() {
        let $span = $("<span></span>");
        this.element.append($span);
        let barWidth = $span.width();
        $span.remove();
        return Math.floor(barWidth);
    }
    get barHeight() {
        let $span = $("<span></span>");
        this.element.append($span);
        let barHeight = $span.height();
        $span.remove();
        return Math.floor(barHeight);
    }

    get barsCountW() {
        let width = this.element.width();
        let barWidth = this.barWidth + this.spacing;
        return Math.floor(width / barWidth);
    }
    get barsCountH() {
        let height = this.element.height();
        let barHeight = this.barHeight + this.spacing;
        return Math.floor(height / barHeight);
    }

    /**
     * @description populates ruler with points
     * @param  {Number} minSize - minimal size
     * @param  {Number} maxSize - maximal size
     * @param  {Object} direction - direction of increasment of size from dirEnum property
     */
    populate(minSize,  maxSize, direction = this.dirEnum.ltr) {
        if (direction == this.dirEnum.ltr || direction == this.dirEnum.rtl) {
            let barCount = this.barsCountW;
            let hDif = maxSize - minSize;
            let hIncre = hDif / barCount;
            let self = this;
            for(let i = 0; i < barCount; i++) {
                let height = direction == this.dirEnum.ltr ? ( ((i +1) * hIncre) + minSize ) : ( maxSize - ((i + 1) * hIncre));
                self.element.append("<span style='height:" + height + "px'></span>");
            }
        } else if (direction == this.dirEnum.ttd || direction == this.dirEnum.dtt) {
            let barCount = this.barsCountH;
            let wDif = maxSize - minSize;
            let wIncre = wDif / barCount;
            let self = this;
            for(let i = 0; i < barCount; i++) {
                let width = direction == this.dirEnum.dtt ? ( ((i +1) * wIncre) + minSize ) : ( maxSize - ((i + 1) * wIncre));
                self.element.append("<span style='width:" + width + "px'></span>");
            }
        }
        
    }
    empty() {
        this.element.empty();
    }
    
}