import App from './AppCore'
import $ from 'jquery'
import {Howl, Howler} from 'howler'
import SoundBoard from './SoundBoard'
import StaticHelpers from './StaticHelpers.js'
import SoundVisualizer from './SoundVisualizer';

export default class TextAnimation {
    constructor(){
        this.soundBoard = null;
        this.visualizer = null;
        this.soundKeys = null;
    }
    loadSoundBoard(board) {
        if (board instanceof SoundBoard) {
            this.soundBoard = board;
        } else {
            throw new Error("Not a soundboard");
        }
    }
    /**
     * @param  {SoundVisualizer} visualizer
     * @param {Array} soundKeys
     */
    loadVisualizer(visualizer, soundKeys) {
        this.visualizer = visualizer;
        this.soundKeys = soundKeys;
    }
    /**
     * @param  {String} text=newString(
     * @param  {Number} timePerLetter=newNumber(
     * @param  {JQuery} element=new$
     */
    Write(text = new String(), timePerLetter = new Number(), element = new $) {
        return new Promise((resolve, reject)=>{
            if (typeof text != "string" || typeof timePerLetter != "number") {
                reject("Wrong input types");
                return;
            }

            let $target = StaticHelpers.ensureJquery(element);

            if (!(text.length > 0)) {
                text = " ";
            }
            if ((!timePerLetter > 0)) {
                timePerLetter = 1;
            }
            let self = this;
            let lettersLeft = text;
            let cursor = $target.attr("data-cursor");
            //interval
            let interval = setInterval(()=>{
                //play stroke
                let i = StaticHelpers.randomBetween(0,this.soundKeys.length);
                let keyRef = this.soundBoard.getSoundReference(this.soundKeys[i]);
                this.visualizer.stop();
                this.visualizer.setSound(keyRef);
                this.visualizer.play();

                //select letter to write
                let letter = lettersLeft.charAt(0);
                lettersLeft = lettersLeft.substr(1);
                //set it
                $target.attr("data-text", $target.attr("data-text").replace(cursor, "") + letter + cursor);
                $target.text($target.text().replace(cursor, "") + letter + cursor );
                

                //if nothing more to write end
                if (lettersLeft.length == 0) {
                    clearInterval(interval);
                    resolve();
                }
            }, timePerLetter);
        });
    }
    Clear(element = new $, pauseBefore = new Number(), pauseAfter = pauseBefore) {
        return new Promise((resolve, reject)=>{
            if (typeof pauseBefore != "number"|| typeof pauseAfter != "number") {
                reject("Wrong input types");
                return;
            }

            let timeout = setTimeout(()=>{
                
                //play stroke
                let keyRef = this.soundBoard.getSoundReference("keystroke1");
                console.log(keyRef);
                this.visualizer.stop();
                this.visualizer.setSound(keyRef);
                this.visualizer.play();

                let $el = StaticHelpers.ensureJquery(element);
                $el.text($el.attr("data-cursor"));
                $el.attr("data-text", $el.attr("data-cursor"));

                clearTimeout(timeout);
                timeout = setTimeout(()=>{
                    clearTimeout(timeout);
                    resolve();
                }, pauseAfter);
                
            }, pauseBefore);
        })
    }
}