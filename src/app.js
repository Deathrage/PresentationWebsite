"use strict";

import $ from 'jquery'
import nunjucks from 'nunjucks'
import ContextHelper from './js/ContextHelper'
import App from './js/AppCore'
import SoundBoard from './js/SoundBoard'
import TextAnimation from './js/TextAnimation'
import Params from './particlesjs-config.js'
import StaticHelpers from './js/StaticHelpers.js'
import SoundVisualizer from './js/SoundVisualizer.js'
import SVGLoader from './js/SVGLoader'
import { startReacting } from './js/react/root';


//fire damn nunjucks ready
nunjucks.configure({
    autoescape: true
});

//prepare soundboard
const soundBoard = new SoundBoard();
soundBoard.loadSounds("assets/sounds/", ["keystroke1.mp3", "keystroke2.mp3", "keystroke3.mp3", "multiplekeystrokes.mp3", "threekeystrokes.mp3", "background.mp3"]);
//create animation object
const textAnimation = new TextAnimation();
textAnimation.loadSoundBoard(soundBoard);
//evaluate right context
const context = new ContextHelper();
context.setLang(StaticHelpers.getQueryString("lang") || "cs");
var contextPromise = context.getLocalization("assets/");
var soundVisualizer;

$(() => {
    //set soundboard
    
    //crate app
    const app = new App($(".js-app")); 
    app.toggleScroll(true);
    //load content to app
    contextPromise.then(()=>{
        app.rednerContent("app.njk", context.localizedContext);

        soundVisualizer = new SoundVisualizer($(".soundvisual"), soundBoard.getSoundReference("background"));
        soundVisualizer.disablePlayPause();
        textAnimation.loadVisualizer(soundVisualizer, ["keystroke1", "keystroke2", "keystroke3"]);

        //app do 
        app.pJSParams = Params;
        app.spawnParticles($(".js-app > .canvas"));
        app.loadParticleConfigurator($(".particle-config"));

        //start text animation
        let $teaserText = $(".js-teaser-text");

        //skip intro
        // app.hideTeaser($teaserText.closest("#blackboard"));
        // app.toggleScroll();
        // soundVisualizer.enablePlayPause();
        // SVGLoader.autoFetch();


        textAnimation.Write("JAVASCRIPT", 200, $teaserText).then(()=>{
            return textAnimation.Clear($teaserText, 1500, 1000);
        }).then(()=>{
            return textAnimation.Write("NODE.JS", 400, $teaserText);
        }).then(()=>{
            return textAnimation.Clear($teaserText, 1000, 700);
        }).then(()=>{
            return textAnimation.Write("ASP.NET", 300, $teaserText);
        }).then(()=>{
            return textAnimation.Clear($teaserText, 1000, 600);
        }).then(()=>{
            return textAnimation.Write("FRONTEND", 200, $teaserText);
        }).then(()=>{
            return textAnimation.Clear($teaserText, 1500, 900);
        }).then(()=>{
            return textAnimation.Write("DEVELOPER", 300, $teaserText);
        }).then(()=>{
            return textAnimation.Clear($teaserText, 2000, 0);
        }).then(()=>{
            soundVisualizer.setSound(soundBoard.getSoundReference("background"));
            soundVisualizer.enablePlayPause();
            soundVisualizer.soundReference.loop(true);
            soundVisualizer.play();
            app.hideTeaser($teaserText.closest("#blackboard"));
            app.toggleScroll();
        });

    
        app.particlesConfigEvents($body);
        app.initSlideHover();
        app.bindSliderHoverEvents($body);
    });
    //setup cofig events
    let $body = $("body");
    //on request set new lang and re-render app
    $body.on("click", ".js-lang", (e) => {

        let $self = $(e.target);
        let lang = $self.text();

        if (context.lang == lang) {
            return;
        }

        //hide app
        var hideProm = app.hideApp(500);
        //get new local
        context.setLang(lang);
        var contextPromise = context.getLocalization("assets/");
        
        hideProm.then(() =>{
            contextPromise.then(()=>{
                app.rednerContent("app.njk", context.localizedContext);
                app.respawnParticles($(".js-app > .canvas"));
                app.loadParticleConfigurator($(".particle-config"));
                app.initSlideHover();

                soundVisualizer.respawn($(".soundvisual"));

                //SVGLoader.autoFetch();

                $(window).trigger("scroll");
                //show new app
                app.showApp(500);   
            });
        });
    });
    $body.on("click", ".js-scroll-section", (e)=>{
        let tH = $(e.target).closest(".scroll-anchor").next().offset().top;
        $('html, body').animate({
            scrollTop: tH - 3
        }, 800);
    });
    $(window).on("scroll", (e)=>{
        let scrollTop = $(window).scrollTop();
        let wH = $(window).height();
        let dH = $(document).height();
        //toper
        let $tar = $(e.target);
        let $topper = $(".topper");
        if ($tar.scrollTop() > 8) {
            $topper.stop();
            $topper.animate({"right": 0}, 500);
        } else {
            $topper.stop();
            $topper.animate({"right": -40}, 500);
        }
        //menu
        let $navIcons = $(".nav-icons .nav-icon");
        $navIcons.removeClass("open");
        // check for footer first
        if (scrollTop + wH >= dH) {
            $(".nav-icons .nav-icon").last().addClass("open");
            return false;
        }
        // check for others
        let $toCheck = $(".section-item");
        $($toCheck.get().reverse()).each((i,e)=>{
            let $tar = $(e);
            if (($tar.offset().top - ($tar.height() / 2)) <= scrollTop) {
                $navIcons.eq(Math.abs(i-$toCheck.length+1)).addClass("open");
                return false;
            }
        });

        return false;
    });
    $body.on("click", ".topper",(e)=>{
        let $tar = $(e.target);
        if ($tar.hasClass("oi")) {
            $tar = $tar.parent(".topper");
        }
        $('html, body').animate({
            scrollTop: 0
        }, 500);

        return false;
    });
    $body.on("click", ".nav-icons .nav-icon", (e)=>{
        let $tar = $(e.target);
        if (!$tar.hasClass(".nav-icon")) {
            $tar = $tar.closest(".nav-icon");
        }
        let i = $tar.index(".nav-icons .nav-icon");
        let $secItems = $(".section-item");
        if (i >= $secItems.length) {
            $("html, body").animate({"scrollTop": $(document).height()}, 500);
        } else {
            $("html, body").animate({"scrollTop": $secItems.eq(i).offset().top + 1}, 500);
        }
        return false;
    });
});

// $(document).on('render', () => {
// 	alert('macho');
// 	startReacting();
// });