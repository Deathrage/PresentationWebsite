const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const config = {
    entry: ['@babel/polyfill', './src/app.js', './src/scss/main.scss'],
    output: {
        filename: "app.js",
        path: path.resolve(__dirname, "dist")
    },
    mode: 'production',
    //mode: 'development',
	//devtool: "cheap-module-source-map",
	resolve: {
		extensions: ['.js', '.jsx']
	},
    module: {
        rules: [
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            { test: /\.(ttf|eot|woff|woff2|otf|svg)$/, 
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            }

        ]
    },
    plugins: [
        new MiniCssExtractPlugin({ // define where to save the file
          filename: '[name].css',
          chunkFilename: "[id].css"
        }),
    ]
};
  
module.exports = config;